package Tests;

import java.util.regex.Pattern;

public class TesteMatch {
    public static void main(String[] args) {
        String cap = "[\\d][\\.][\\s].+";
        Pattern pattern =  Pattern.compile(cap);
        String sec = "[\\d][\\.][\\d][\\.][\\s].+";
        Pattern pattern1 = Pattern.compile(sec);
        String subSec = "[\\d][\\.][\\d][\\.][\\d][\\.][\\s].+";
        Pattern pattern2 = Pattern.compile(subSec);
        String par = "[\\d]+";
        Pattern pattern3 = Pattern.compile(par);
        String a = "1. Introdução";
        String b = "1.1. Conceitos Básicos";
        String c = "3";
        String d = "2";
        String e = "1.2. Preparação do Ambiente";
        String f = "2";
        String g = "1.2.1. Requisitos Mínimos";
        String h = "9";
        String i = "1.2.2. Instalação";
        System.out.println("A: CAPITULO");
        System.out.println("Match cap: " + a.matches(cap));
        System.out.println("Match sec: " + a.matches(sec));
        System.out.println("Match sub: " + a.matches(subSec));
        System.out.println("Match par: " + a.matches(par));

        System.out.println("B: Seção");
        System.out.println("Match cap: " + b.matches(cap));
        System.out.println("Match sec: " + b.matches(sec));
        System.out.println("Match sub: " + b.matches(subSec));
        System.out.println("Match par: " + b.matches(par));

        System.out.println("C: Parágrafo");
        System.out.println("Match cap: " + c.matches(cap));
        System.out.println("Match sec: " + c.matches(sec));
        System.out.println("Match sub: " + c.matches(subSec));
        System.out.println("Match par: " + c.matches(par));

        System.out.println("G: SubSeção");
        System.out.println("Match cap: " + g.matches(cap));
        System.out.println("Match sec: " + g.matches(sec));
        System.out.println("Match sub: " + g.matches(subSec));
        System.out.println("Match par: " + g.matches(par));
    }
}
