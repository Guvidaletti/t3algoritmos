import java.util.ArrayList;

public class BookTreeOfString {

    //Classe interna
    private class Node{

        public Node pai;
        public String elemento;
        public ArrayList<Node> subArvores;

        public Node(String elemento){
            pai=null;
            this.elemento = elemento;
            subArvores = new ArrayList<>();
        }

        public void addSubArvore(Node n){
            n.pai = this;
            subArvores.add(n);
        }

        public boolean removeSubArvore(Node n){
            n.pai = null;
            return subArvores.remove(n);
        }

        public Node getSubArvore(int i){
            if((i<0) || (i>=subArvores.size())){
                throw new IndexOutOfBoundsException();
            }
            return subArvores.get(i);
        }

        public int getTamanhhoSubArvores(){
            return subArvores.size();
        }
    }

    // Atributos
    private Node raiz;
    private int contador;

    // Metodos
    public BookTreeOfString(){
        raiz=null;
        contador=0;
    }

    public String getRaiz(){
        if(estaVazio()){
            throw new EmptyTreeException();
        }
        return raiz.elemento;
    }

    public void setRaiz(String elemento){
        if(estaVazio()){
            throw new EmptyTreeException();
        }
        raiz.elemento = elemento;
    }

    public boolean ehRaiz(String elemento){
        if(raiz!= null){
            if (raiz.elemento.equals(elemento)) {
                return true;
            }
        }
        return false;
    }

    public boolean estaVazio(){
        return (raiz == null);
    }

    public int tamanho(){
        return contador;
    }

    public void limpar(){
        raiz = null;
        contador = 0;
    }

    public String getPai(String elemento){
        Node n = procurarReferenciaNodo(elemento,raiz);
        if((n==null)||(n.pai==null)){
            return null;
        } else {
            return n.pai.elemento;
        }
    }

    public boolean contem(String elemento) {
        Node nAux = procurarReferenciaNodo(elemento,raiz);
        return (nAux!=null);
    }

    private Node procurarReferenciaNodo(String elemento, Node alvo){
        Node res = null;
        if(alvo!=null){
            if(elemento.equals(alvo.elemento)){
                res = alvo;
            } else {
                Node aux = null;
                int i = 0;
                while((aux==null) && (i<alvo.getTamanhhoSubArvores())){
                    aux = procurarReferenciaNodo(elemento,alvo.getSubArvore(i));
                    i++;
                }
                res = aux;
            }
        }
        return res;
    }

    public boolean adicionar(String elemento,String pai){
        Node n = new Node(elemento);
        Node nAux = null;
        boolean res = false;
        if(pai==null){ //inserimos na raiz
            if (raiz != null){//Atualizamos o pai da raiz
                n.addSubArvore(raiz);
                raiz.pai=n;
            }
            raiz = n; // atualizamos a raiz
            res = true;
        } else { // inserimos no meio da arvore
            nAux = procurarReferenciaNodo(pai,raiz);
            if (nAux!=null){
                nAux.addSubArvore(n);
                res = true;

            }
        }
        contador++;
        return res;
    }

    public ArrayList<String> posicoesPre(){
        ArrayList<String> lista = new ArrayList<>();
        posicoesPreAux(raiz,lista);
        return lista;
    }

    private void posicoesPreAux(Node n,ArrayList<String> lista){
        if (n!=null) {
            lista.add(n.elemento);
            for(int i = 0 ; i< n.getTamanhhoSubArvores();i++){
                posicoesPreAux(n.getSubArvore(i),lista);
            }
        }
    }

    public ArrayList<String> TamanhoPosicoes(){
        ArrayList<String> lista = new ArrayList<>();

        Queue<Node> fila = new Queue<>();
        Node atual;

        if(raiz!=null){
            fila.enqueue(raiz);
            while(!fila.isEmpty()){
                atual = fila.dequeue();
                lista.add(atual.elemento);
                for(int i=0;i<atual.getTamanhhoSubArvores();i++){
                    fila.enqueue(atual.getSubArvore(i));
                }
            }
        }
        return lista;
    }


}
