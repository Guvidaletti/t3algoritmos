import java.awt.print.Book;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.FormatFlagsConversionMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Livro {

    private BookTreeOfString arvore;
    private int capAtual = 0,secAtual=0,subSecAtual=0,totalDePaginas=1;
    private int cTotal=0,sTotal=0,ssTotal=0,pTotal=0;
    private ArrayList<String> sumario,livro,conteudo;
    private String Titulo="";


    public Livro(String nomeEntrada,String nomeSaida) throws IOException {
        livro = new ArrayList<>();
        System.out.print("Carregando arquivo "+nomeEntrada);
        carregarArquivo(nomeEntrada);
        System.out.println("  Capitulos...: " + cTotal);
        System.out.println("  Seções......: " + sTotal);
        System.out.println("  Subseções...: " + ssTotal);
        System.out.println("  Parágrafos..: " + pTotal);
        System.out.print("Gerando o Sumário");
        geraSumario();
        System.out.print("Imprimindo o livro para o arquivo " + nomeSaida);
        geraLivro(nomeSaida);
    }

    public void carregarArquivo(String nomeArq) throws IOException {
        try {for(int i=0;i<3;i++){System.out.print(".");Thread.sleep(600);}}
        catch (InterruptedException e) {e.printStackTrace();}
        Path path = Paths.get("livros/" + nomeArq);
        try (Scanner sc = new Scanner(Files.newBufferedReader(path,
                Charset.forName("utf8")))) {
            System.out.println(" ok");
            System.out.print("Gerando a Arvore");
            try {for(int i=0;i<3;i++){System.out.print(".");Thread.sleep(600);}}
            catch (InterruptedException e) {e.printStackTrace();}
            //Separador: NOVA LINHA
            sc.useDelimiter("(\n)");
            Titulo = "";
            String ultimoCap = "";
            String ultimaSec = "";
            String ultimaSubSec = "";
            arvore = new BookTreeOfString();

            while (sc.hasNext()) {
                String linha = sc.next();
                linha = linha.replaceAll("(\r)", ""); //VERIFICAR NECESSIDADE
                //System.out.println("'"+linha+"'");
                String linhaSemID = "";
                if (linha.charAt(1) == 'L') { //Titulo
                    //System.out.println("ACHEI O TITULO");
                    linhaSemID = linha.replace("L ", "");
                    arvore.adicionar(linhaSemID, null);
                    Titulo = linhaSemID;

                } else if (linha.charAt(0) == 'C') { //Capitulo
                    //System.out.println("ACHEI um capitulo");
                    capAtual++;
                    secAtual=0;
                    subSecAtual=0;
                    linhaSemID = linha.replace("C ", "");
                    String linhaComIndice = capAtual + ". " + linhaSemID;
                    arvore.adicionar(linhaComIndice, Titulo);
                    ultimoCap = linhaComIndice;
                    ultimaSec = "";
                    ultimaSubSec = "";
                    cTotal++;

                } else if (linha.charAt(0) == 'S') {
                    if (linha.charAt(1) == ' ') { //Secao
                        secAtual++;
                        subSecAtual=0;
                        //System.out.println("ACHEI uma secao");
                        linhaSemID = linha.replace("S ", "");
                        String linhaComIndice = capAtual+"."+secAtual+". " + linhaSemID;
                        arvore.adicionar(linhaComIndice, ultimoCap);
                        ultimaSec = linhaComIndice;
                        ultimaSubSec = "";
                        sTotal++;
                    }

                    if (linha.charAt(1) == 'S') { //Subsecao
                        //System.out.println("Achei uma Subsecao");
                        subSecAtual++;
                        linhaSemID = linha.replace("SS ", "");
                        String linhaComIndice = capAtual+"."+secAtual+"."+subSecAtual+". "+linhaSemID;
                        arvore.adicionar(linhaComIndice, ultimaSec);
                        ultimaSubSec = linhaComIndice;
                        ssTotal++;
                    }

                } else if (linha.charAt(0) == 'P') { //Paragrafo
                    linhaSemID = linha.replace("P ", "");
                    if (!ultimaSubSec.equals("")) arvore.adicionar(linhaSemID, ultimaSubSec);
                    else if (!ultimaSec.equals("")) arvore.adicionar(linhaSemID, ultimaSec);
                    else if (!ultimoCap.equals("")) arvore.adicionar(linhaSemID, ultimoCap);
                    else arvore.adicionar(linhaSemID, Titulo); //VERIFICAR POIS É FILHO DO TITULO, NAO SEI SE PODE...
                    pTotal++;
                }
            }
        }
        System.out.println(" ok");
//        arvore.posicoesPre().forEach(n-> System.out.println(n));
    }


    private final String cap = "[\\d][\\.][\\s].+";
    private final Pattern pattern =  Pattern.compile(cap);
    private final String sec = "[\\d][\\.][\\d][\\.][\\s].+";
    private final Pattern pattern1 = Pattern.compile(sec);
    private final String subSec = "[\\d][\\.][\\d][\\.][\\d][\\.][\\s].+";
    private final Pattern pattern2 = Pattern.compile(subSec);
    private final String par = "[\\d]+";
    private final Pattern pattern3 = Pattern.compile(par);
    public void geraSumario(){
        sumario = new ArrayList<>();
        try {for(int i=0;i<3;i++){System.out.print(".");Thread.sleep(600);}}
        catch (InterruptedException e) {e.printStackTrace();}
        int lAtual=1,cont=0,pgAtual=1;
        String titulo=arvore.getRaiz();
        sumario.add("SUMÁRIO");
        for(String s: arvore.posicoesPre()){
            if(cont!=0){ //se não for o título
                String novaLinha="";
                if(s.matches(cap)){//se for capitulo
                    novaLinha = s;
                    if(lAtual!=1){
                        pgAtual++;
                        lAtual=2;
                    }
                    else lAtual=2;

                }
                else if(s.matches(sec)){ //se for secao
                    novaLinha=" " + s;
                    lAtual++;
                    if(lAtual>=15){
                        lAtual = lAtual-15;
                        pgAtual++;
                    }

                }
                else if(s.matches(subSec)){ //se for Subsecao
                    novaLinha = "  " + s;
                    lAtual++;
                    if(lAtual>=15){
                        lAtual = lAtual-15;
                        pgAtual++;
                    }

                }
                else if(s.matches(par)){ //se for parágrafo
                    int addLinhas = Integer.parseInt(s);
                    lAtual+=addLinhas;
                    if(lAtual>=15){
                        lAtual = lAtual-15;
                        pgAtual++;
                    }
                }

                if(!s.matches(par)){ //pra adicionar no sumário; Paragrafo nao adiciona.
                    String ponto = "";
                    String formatada="";
                    int w = 36 - novaLinha.length() - String.valueOf(pgAtual).length();
                    for(int i=1;i<=w;i++)ponto+=".";
                    try{
                        formatada = String.format("%s%"+w+"s%s",novaLinha,ponto,pgAtual);
                    }catch (FormatFlagsConversionMismatchException e){
                        System.err.print("Erro nesta linha: ");
                        System.err.println(novaLinha);
                        System.err.println("Tente reescreve-la");
                    }
                    sumario.add(formatada);
                }
            }
            cont++;
        }
        System.out.println(" ok.");
    }

    private static String centralizarString (int tamanho, String s) {
        return String.format("%-" + tamanho  + "s", String.format("%" + (s.length() + (tamanho - s.length()) / 2) + "s", s));
    }

    private void criaCapa(){
        livro.add("------------------------------------"); //w=36
        for(int i=1;i<=15;i++){
            String n = i+"";
            if(i==7){
                n = centralizarString(36,Titulo);
                n=n.replaceFirst("\\s","7");
            }
            livro.add(n);
        }
        livro.add("------------------------------\tCapa");
    }

    private String separador(int pg){
        int w = 36;
        String traco="";
        String id = "Pg. " +String.valueOf(pg);
        w=36-id.length();
        for(int i=1;i<=w;i++)traco+="-";
        String res = String.format("%s %s",traco,id);
        return res;


    }

    private void preencheLivro(){
        conteudo = new ArrayList<>();
        int cont=0,linhaAtual=1,pgAtual=1;//modelo: ADICIONA NA LINHA ATUAL , DPS VERIFICA O 15
        String novaLinha = "";
        for(String s: arvore.posicoesPre()){
            if(cont!=0){//pular o titulo
            if(s.matches(par)){//se for um paragrafo digitado
                int limit = Integer.parseInt(s);
                for (int i=1;i<=limit;i++){
                    conteudo.add(linhaAtual+"\tLorem Ipsum "+i);
                    if(linhaAtual==15){
                        conteudo.add(separador(pgAtual));
                        linhaAtual=0;//0 pq "1" vai ser adicionado ali em baixo
                        pgAtual++;
                        totalDePaginas++;
                    }
                    linhaAtual++;
                }
            }

            else if(s.matches(subSec)||s.matches(sec)){//se for secao ou subsecao
                conteudo.add(linhaAtual+"\t"+s);
                if(linhaAtual==15){
                    conteudo.add(separador(pgAtual));
                    linhaAtual=0;
                    pgAtual++;
                    totalDePaginas++;
                }
                linhaAtual++;
            }

            else if(s.matches(cap)){
                if(linhaAtual==1){
                    conteudo.add(linhaAtual+"\t"+s);
                    linhaAtual++;
                }
                else{
                    //int limit = (15-linhaAtual);
                    for(int i=linhaAtual;i<=15;i++){
                        conteudo.add(linhaAtual+"\t~");
                        if(linhaAtual==15){
                            conteudo.add(separador(pgAtual));
                            linhaAtual=0;
                            pgAtual++;
                            totalDePaginas++;
                        }
                        linhaAtual++;
                    }
                    conteudo.add(linhaAtual+"\t"+s);
                    linhaAtual++;
                }
            }
            }
            cont++;
        }
    }

    public void geraLivro(String nomeArq) throws IOException{
        try{for(int i=0;i<3;i++){System.out.print(".");Thread.sleep(600);}}catch(InterruptedException e){e.printStackTrace();}
    File imp = new File("impressao");
        imp.mkdir();
        Path path1 = Paths.get("impressao/" + nomeArq);
        criaCapa();
        preencheLivro();
        conteudo.forEach(s -> livro.add(s));
        sumario.forEach(s -> livro.add(s));
        try(PrintWriter pw = new PrintWriter(Files.newBufferedWriter(path1,Charset.forName("utf8")))) {
            livro.forEach(s->{pw.println(s);});
            }
        System.out.println(" ok.");
    }

}
