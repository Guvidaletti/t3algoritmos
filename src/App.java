import java.io.File;
import java.io.IOException;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        System.out.println("--------------------------");
        System.out.print("Carregando os arquivos da pasta 'livros'");
        try {for(int i=0;i<4;i++){Thread.sleep(400);System.out.print(".");}} catch (InterruptedException e) {e.printStackTrace();}
        System.out.println("ok");
        System.out.println("--------------------------");
        System.out.println("Os livros disponíveis são:");
        File diretorio = new File("livros");
        File[] nome = diretorio.listFiles();
        for(File f:nome)System.out.println("'"+f.getName()+"'");
        System.out.println("--------------------------");

        System.out.println("Atenção, necessita estar codificado em 'UTF-8'");
        System.out.println("--------------------------");
        System.out.print("Digite o nome do arquivo de entrada: ");
        String entrada = teclado.next();

        System.out.print("Digite o nome do arquivo de saída do livro: ");
        String saida = teclado.next();
        System.out.println("--------------------------");

        System.out.println("Seu Livro estará pronto na pasta 'Impressao'");
        System.out.println("--------------------------");

        try {
            Livro livro = new Livro(entrada,saida);
        } catch (IOException e) {
            System.err.println("Erro de arquivo!");
        }

    }
}
